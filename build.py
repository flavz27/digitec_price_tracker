from datetime import datetime

articles = []
final_table = {}

# Get actual articles
with open('./articles.csv', 'r', encoding='utf-8') as articles_file:
    for line in articles_file:
        line_cut = line.split(',')
        if line_cut[0] != 'id':
            articles.append(line_cut[0])

# Build cross table of date / articles
for article in articles:
    with open('./articles/' + article + '.csv', 'r', encoding='utf-8') as values_file:
        for line in values_file:
            line_cut = line.split(',')
            date = datetime.strptime(line_cut[0], '%Y-%m-%d %H:%M:%S.%f').strftime("%Y-%m-%d_%H:%M")
            price = line_cut[1].replace('\n', '').replace('\r', '')
            final_table.setdefault(date, {}).setdefault(article, price)

sorted(final_table)

# Final data file
with open('./data.csv', 'w+', encoding='utf-8') as data_file:
    # First line
    for article in articles:
        data_file.write(',' + article)
    data_file.write('\n')

    # Test for each price of each date (line), if there is a price for a given article
    for date, articles_price in final_table.items():
        data_file.write(datetime.strptime(date, '%Y-%m-%d_%H:%M').strftime("%d.%m.%Y %H:%M"))
        for article in articles:
            if article in articles_price:
                data_file.write("," + articles_price[article])
            else:
                data_file.write(",")
        data_file.write('\n')
