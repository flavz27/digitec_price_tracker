# digitec price tracker

This project can be used to track prices on your [digitec](digitech.ch) wishlist. 

## Usage
```bash
python start.py [WISHLIST_ID]
```

Your `WISHLIST_ID` can be found in the sharing url of your wishlist.

```https://www.digitec.ch/fr/ShopList/Show?shopListId=WISHLIST_ID```


The scripts will create an `articles` directory containing one file per article in your wishlist. It will then aggregate all the information into `data.csv`. 

A key containing the ID, the corresponding names and url to each articles can be found in `articles.csv`.

If you remove an article from your wishlist, it will also be removed from `data.csv` and `articles.csv`.

