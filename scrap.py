import requests
from bs4 import BeautifulSoup
from datetime import datetime
import sys

shopListId = sys.argv[1]

url = "https://www.digitec.ch/fr/ShopList/Show?shopListId="+shopListId
page = requests.get(url, headers={'User-Agent': 'Custom'}).text
soup = BeautifulSoup(page, 'html.parser')

# prices = []
# names = []
# urls = []
# ids = []
#
# for article in soup.select("article.panel"):
#     name = article.select("h5.product-name")[0].get_text().replace('\r\n',' ').strip()
#     names.append(name)
#     price = article.select("div.product-price")[0].get_text().split('avant', 1)[0].strip()
#     prices.append(price)
#     url = article.select("a")[0]['href']
#     urls.append(url)
#     id = article['data-product-id']
#     ids.append(id)

# Build articles index
with open('./articles.csv', 'w+', encoding='utf-8') as article_file:
    article_file.write('id,name,url\n')

    for article in soup.select("article.panel"):
        name = article.select("h5.product-name")[0].get_text().replace('\r\n', ' ').strip().replace(','," ")
        price = article.select("div.product-price")[0].get_text().split('avant', 1)[0].strip().split('jeu', 1)[0].strip().replace('CHF ', '').replace('–', '00')
        url = article.select("a")[0]['href']
        id = article['data-product-id']
    

        # Build price history for each article in a dedicated file
        with open('./articles/' + id + '.csv', 'a+', encoding='utf-8') as values_file:
            values_file.write(str(datetime.today()) + "," + price + "\n")

        article_file.write(str(id) + "," + str(name) + "," + url + '\n')
     

